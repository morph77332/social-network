export const updateObjectInArray = (items, itemId, objProp, newObjProps) => {
    return items.map(u => {
        if (u[objProp] === itemId) {
            return { ...u, ...newObjProps }
        }
        return u;
    })
}