import { newsAPI } from "../api/api";

const SET_NEWS = 'SET_NEWS';
const TOGGLE_IS_FETCHING = 'TOGGLE-IS-FETCHING';

let initialState = {
    data: [],
    fetching: false
};

const PostsReducer = (state = initialState, action) => {

    switch (action.type) {
        case SET_NEWS:
            return {
                ...state, data: [...state.data, ...action.news]
            }
        case TOGGLE_IS_FETCHING:
            return {
                ...state,
                isFetching: action.isFetching
            }
        default:
            return state;
    }
}


export const setNews = (news) => ({ type: SET_NEWS, news });
export const toggleIsFetching = (fetching) => {
    return {
        type: TOGGLE_IS_FETCHING,
        fetching
    }
}

export const getNews = () => async (dispatch) => {
    dispatch(toggleIsFetching(true));
    const data = await newsAPI.getNews();
    dispatch(toggleIsFetching(false));
    dispatch(setNews(data));
}


export default PostsReducer;
