import user from './../assets/images/user.png'
const SEND_MESSAGE = 'SEND-MESSAGE';

let initialState = {
    dialogs: [
        { id: 1, name: 'user', text: 'Hi!', avatar: user, time: '5:28 pm' },
        { id: 2, name: 'user', text: 'What\'s up bro?', avatar: user, time: '2:08 pm' },
        { id: 3, name: 'user', text: '(:', avatar: user, time: '11:56 am' },
        { id: 4, name: 'user', text: 'go to the cinema', avatar: user, time: '10:11 am' },
        { id: 5, name: 'user', text: 'lol', avatar: user, time: '10:11 am' }


    ],
    friendInfo: [
        { id: 1, name: 'user', avatar: user, job: 'Works at ', city: 'City, RuCountryssia' },
        { id: 2, name: 'user', avatar: user, job: 'Works at ', city: 'City, Country' },
        { id: 3, name: 'user', avatar: user, job: 'Works at ', city: 'City, Country' },
        { id: 4, name: 'user', avatar: user, job: 'Studie in', city: 'City, Country' },
        { id: 5, name: 'user', avatar: user, job: 'Works at ', city: 'City, Country' },
    ],
    messages: []
}

const MessengerReducer = (state = initialState, action) => {

    switch (action.type) {
        case SEND_MESSAGE: 
            let newMessage = {
                id: 1,
                text: action.text,
                img: user
            }
            return {
                ...state,
                messages: [...state.messages, newMessage]
            };
        default: {
            return state;

        }
    }
}

export const sendMessageActionCreator = (text) => {
    return {
        type: SEND_MESSAGE,
        text
    }
}

export default MessengerReducer;