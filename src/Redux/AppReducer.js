import { setAuthUserProfileThunk } from "../Redux/AuthReducer";

const INITIAL_SUCCESS = 'INITIAL_SUCCESS';

let initialState = {
    initialised: false
};

const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case INITIAL_SUCCESS:
            return {
                ...state,
                initialised: true
            }
        default:
            return state;
    }
}

export const initialSuccess = () => ({ type: INITIAL_SUCCESS })

export const initialApp = () => (dispatch) => {
    let promise = dispatch(setAuthUserProfileThunk());
    promise.then(() => {
        dispatch(initialSuccess())
    })
}

export default appReducer;