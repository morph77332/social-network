import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import ProfilesReducer from "./ProfileReducer";
import MessengerReducer from "./MessengerReducer";
import FriendsReducer from "./FriendsReducer";
import authReducer from "./AuthReducer";
import NewsReducer from "./NewsReducer";
import thunkMiddleware from "redux-thunk";
import { reducer as formReducer } from 'redux-form';
import appReducer from "./AppReducer";


let reducer = combineReducers({
    main: ProfilesReducer,
    messenger: MessengerReducer,
    friends: FriendsReducer,
    auth: authReducer,
    form: formReducer,
    app: appReducer,
    news: NewsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

// let store = createStore(reducer, applyMiddleware(thunkMiddleware));

window.store = store;

export default store;