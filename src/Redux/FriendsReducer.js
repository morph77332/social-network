import { usersAPI } from '../api/api';
import { updateObjectInArray } from '../utils/object-helper';

const ADD_FRIEND = 'ADD-FRIEND';
const REMOVE_FRIEND = 'REMOVE-FRIEND';
const SET_USERS = 'SET-USERS';
const SET_CURRENT_PAGE = 'SET-CURRENT-PAGE';
const SET_TOTAL_COUNT = 'SET-TOTAL-COUNT';
const TOGGLE_IS_FETCHING = 'TOGGLE-IS-FETCHING';
const PROGRESS_ADD = 'PROGRESS-ADD';

let initialState = {
    users: [],
    pageSize: 6,
    totalUsersCount: 0,
    currentPage: 1,
    isFetching: false,
    progressAdd: [],
};

const FriendsReducer = (state = initialState, action) => {

    switch (action.type) {
        case ADD_FRIEND:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.userId, 'id', { followed: true })
            }
        case REMOVE_FRIEND:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.userId, 'id', { followed: false })
            }
        case SET_USERS:
            return { ...state, users: [...action.users] }
        case SET_CURRENT_PAGE:
            return { ...state, currentPage: action.currentPage }
        case SET_TOTAL_COUNT:
            return { ...state, totalUsersCount: action.totalUsersCount }
        case TOGGLE_IS_FETCHING:
            return {
                ...state,
                isFetching: action.isFetching
            }
        case PROGRESS_ADD:
            return {
                ...state,
                progressAdd: action.progressAdd ? [...state.progressAdd, action.userId] : state.progressAdd.filter(id => id !== action.userId)
            }
        default:
            return state;
    }
}

export const addFriend = (userId) => {
    return {
        type: ADD_FRIEND,
        userId: userId
    }
}

export const removeFriend = (userId) => {
    return {
        type: REMOVE_FRIEND,
        userId: userId
    }
}

export const setUsers = (users) => {
    return {
        type: SET_USERS,
        users
    }
}

export const setCurrentPage = (currentPage) => {
    return {
        type: SET_CURRENT_PAGE,
        currentPage
    }
}

export const setTotalCount = (totalUsersCount) => {
    return {
        type: SET_TOTAL_COUNT,
        totalUsersCount
    }
}

export const toggleIsFetching = (isFetching) => {
    return {
        type: TOGGLE_IS_FETCHING,
        isFetching
    }
}

export const toggleProgressAdd = (progressAdd, userId) => {
    return {
        type: PROGRESS_ADD,
        progressAdd,
        userId
    }
}

const followUnfollowFlow = async (dispatch, userId, apiMethod, actionCreator) => {
    dispatch(toggleProgressAdd(true, userId));
    const response = await apiMethod(userId);
    if (response.data.resultCode === 0) {
        dispatch(actionCreator(userId))
    };
    dispatch(toggleProgressAdd(false, userId))
}

export const getUsersThunk = (currentPage, pageSize) => async (dispatch) => {
    dispatch(toggleIsFetching(true));
    dispatch(setCurrentPage(currentPage));
    const data = await usersAPI.getUsers(currentPage, pageSize);
    dispatch(toggleIsFetching(false));
    dispatch(setUsers(data.items));
    dispatch(setTotalCount(data.totalCount))
}


export const follow = (userId) => async (dispatch) => {
    followUnfollowFlow(dispatch, userId, usersAPI.follow, addFriend)
}

export const unFollow = (userId) => async (dispatch) => {
    followUnfollowFlow(dispatch, userId, usersAPI.unfollow, removeFriend)
}


export default FriendsReducer;
