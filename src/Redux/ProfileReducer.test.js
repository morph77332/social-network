import PostsReducer, { addPostActionCreator, deletePost } from "./ProfileReducer";

let state = {
    posts: [
        { id: 1, time: 56, text: 'Love, death + robots', img: 'https://fsmedia.imgix.net/b4/d4/c8/64/f967/4780/a435/12de2f09863c/did-we-really-need-this-joke.png?rect=70%2C0%2C1030%2C515&auto=format%2Ccompress&dpr=2&w=650', likes: 264 },
        { id: 2, time: 18, text: 'Your second post!', img: 'https://xage.ru/media/posts/2019/7/24/exclusive-brutal-clip-from-adult-swim-shows-primal-scene.jpg', likes: 321 },
        { id: 3, time: 1, text: 'Your post message text goes in this area, you can edit it easily according to your needs, enjoy.', img: 'https://free4kwallpapers.com/uploads/originals/2018/03/08/rick-sanchez-easier-for-you-wallpaper.jpg', likes: 28 }
    ],
};

test('test add post', () => {
    // test data
    let action = addPostActionCreator('test is work');
    // action
    let newState = PostsReducer(state, action);
    // expectation
    expect(newState.posts.length).toBe(4);
});

test('test new post text', () => {
    // test data
    let action = addPostActionCreator('test is work');
    // action
    let newState = PostsReducer(state, action);
    // expectation
    expect(newState.posts[3].text).toBe('test is work');
});

test('delete post', () => {
    // test data
    let action = deletePost(1);
    // action
    let newState = PostsReducer(state, action);
    // expectation
    expect(newState.posts.length).toBe(2);
});

