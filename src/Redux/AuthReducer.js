import { authAPI } from "../api/api";
import { stopSubmit } from 'redux-form';

const SET_USER_DATA = 'SET_USER_DATA';
const SET_USER_PROFILE = 'SET-USER-PROFILE';

let initialState = {
    userId: null,
    email: null,
    login: null,
    isAuth: false,
    authUserProfile: null,
    sidebar: false,
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return { ...state, ...action.data, isAuth: action.isAuth }
        case SET_USER_PROFILE:
            if (!action.authUserProfile) {
                return state
            }
            return {
                ...state,
                authUserProfile: action.authUserProfile
            }
        default:
            return state;
    }
}

export const setAuthUserData = (userId, email, login, isAuth) => ({
    type: SET_USER_DATA,
    data: {
        userId,
        email,
        login,
    },
    isAuth,
})

export const setAuthUserProfile = (authUserProfile) => {
    return {
        type: SET_USER_PROFILE,
        authUserProfile
    }
}

export const setAuthUserProfileThunk = () => async (dispatch) => {
    const response = await authAPI.me();
    if (response.resultCode === 0) {
        let { email, id, login } = response.data;
        dispatch(setAuthUserData(id, email, login, true));
        authAPI.getAuthProfile(id).then(response => {
            dispatch(setAuthUserProfile(response))
        })
    }
}


export const loginThunk = (email, password, rememberMe) => async (dispatch) => {
    const response = await authAPI.login(email, password, rememberMe);
    if (response.data.resultCode === 0) {
        dispatch(setAuthUserProfileThunk())
    } else {
        let message = response.data.messages.length > 0 ? response.data.messages[0] : 'Email or password is wrong'
        dispatch(stopSubmit('login', { _error: message }))
    }
}

export const logOutThunk = () => async (dispatch) => {
    const response = await authAPI.logOut();
    if (response.data.resultCode === 0) {
        dispatch(setAuthUserData(null, null, null, false));
    }
}


export default authReducer;