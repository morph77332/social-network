import { profileAPI } from "../api/api";
import post from './../assets/images/post.png'
import { setAuthUserProfileThunk } from "./AuthReducer";

const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';
const SET_USER_PROFILE = 'SET-USER-PROFILE';
const SET_STATUS = 'SET-STATUS';
const INITIAL_PROFILE_SUCCESS = 'INITIAL_PROFILE_SUCCESS';
const DELETE_POST = 'DELETE_POST';
const SAVE_PHOTO_SUCCESS = 'SAVE_PHOTO_SUCCESS';
const PRELOADER = 'PRELOADER';
const SAVE_PROFILE = 'SAVE_PROFILE';
const SET_EDITMODE = 'SET_EDITMODE';

let initialState = {
    posts: [
        { id: 1, time: 56, text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.  remaining essentially unchanged.', img: post, likes: 264 },
        { id: 2, time: 18, text: 'when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting', img: post, likes: 321 },
        { id: 3, time: 1, text: 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', likes: 28 }
    ],
    profile: null,
    status: null,
    initial: false,
    preloader: false,
    editMode: false
};

const PostsReducer = (state = initialState, action) => {

    switch (action.type) {
        case ADD_POST:
            let newPost = {
                id: 4,
                time: 0,
                text: action.text,
                likes: 0
            }
            return {
                ...state, posts: [...state.posts, newPost]
            }
        case DELETE_POST:
            return {
                ...state, posts: state.posts.filter(p => p.id !== action.postId)
            }
        case SET_USER_PROFILE:
            if (action.profile === undefined) {
                return state
            }
            return {
                ...state,
                profile: action.profile
            }
        case SET_STATUS:
            return {
                ...state,
                status: action.status
            }
        case SAVE_PHOTO_SUCCESS:
            return {
                ...state,
                profile: { ...state.profile, photos: action.photos }
            }
        case PRELOADER:
            return {
                ...state,
                preloader: action.preloader
            }
        case INITIAL_PROFILE_SUCCESS:
            return { ...state, initial: true }
        case SET_EDITMODE:
            return {
                ...state,
                editMode: action.editMode
            }
        default:
            return state;
    }
}


export const addPost = (text) => ({ type: ADD_POST, text });
export const deletePost = (postId) => ({ type: DELETE_POST, postId });
export const updateNewPost = (text) => ({ type: UPDATE_NEW_POST_TEXT, newText: text });
export const setUserProfile = (profile) => ({ type: SET_USER_PROFILE, profile });
export const setStatus = (status) => ({ type: SET_STATUS, status });
export const savePhotoSuccess = (photos) => ({ type: SAVE_PHOTO_SUCCESS, photos });
export const preloaderPhoto = (preloader) => ({ type: PRELOADER, preloader });
export const setsaveProfile = (profile) => ({ type: SAVE_PROFILE, profile });
export const setAboutEditMode = (editMode) => ({type: SET_EDITMODE, editMode});

export const getProfilethunk = (userId) => async (dispatch) => {
    const response = await profileAPI.getProfile(userId);
    dispatch(setUserProfile(response))
}

export const getUserStatus = (userId) => async (dispatch) => {
    const response = await profileAPI.getStatus(userId);
    dispatch(setStatus(response.data))
}

export const updateStatus = (status) => async (dispatch) => {
    const response = await profileAPI.updateStatus(status);
    if (response.data.resultCode === 0) {
        dispatch(setStatus(status))
    }
}

export const savePhoto = (file) => async (dispatch) => {
    dispatch(preloaderPhoto(true));
    const response = await profileAPI.savePhoto(file);
    if (response.data.resultCode === 0) {
        dispatch(savePhotoSuccess(response.data.data.photos));
        dispatch(preloaderPhoto(false));
    }
}

export const saveProfile = (profile) => async (dispatch) => {
    const response = await profileAPI.saveProfile(profile);
    if (response.data.resultCode === 0) {
        dispatch(setAboutEditMode(false))
    }
}

export default PostsReducer;
