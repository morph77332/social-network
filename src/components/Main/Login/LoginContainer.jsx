import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { setAuthUserProfileThunk } from './../../../Redux/AuthReducer';
import Login from './Login';

const LoginContainer = ({ isAuth }) => {
    if (isAuth) {
        return <Redirect to={'/news'} />
    } else {
        return (
            <Login />
        )
    }
}

let mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth
})

export default connect(mapStateToProps, { setAuthUserProfileThunk })(LoginContainer);