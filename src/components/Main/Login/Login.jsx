import React from 'react';
import style from './Login.module.css'


const Login = () => {
    return (
        <div className={style.loginWrapper}>
            <div className={style.loginPageImgWrapper}>
                <h4>Facebook helps you connect and share with the people in your life.</h4>
                <div className={style.loginPageimg}>
                    <img src="https://static.xx.fbcdn.net/rsrc.php/v3/yi/r/OBaVg52wtTZ.png" alt="pic"/>
                </div>
            </div>
            <div className={style.singUpBlockWrapper}>
                <div className={style.singUpTextBlock}>
                    <h1>Create an account</h1>
                    <span>It's quick and easy.</span>
                </div>
                <div className={style.singUpBlock}>
                    <div>
                        <span>First Name:</span> <input type="text" />
                    </div>
                    <div>
                        <span>Last Name:</span> <input type="text" />
                    </div>
                    <div>
                        <span>Your Email:</span> <input type="text" />
                    </div>
                    <div>
                        <span>Re-enter Email:</span> <input type="text" />
                    </div>
                    <div>
                        <span>New Password:</span> <input type="password" />
                    </div>
                    <div className={style.selectBlock}>
                        <span>Birthday:</span>
                        <select name="" id="">
                            <option value="">Month</option>
                            <option value="">January</option>
                            <option value="">Ferbruary</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                        </select>
                        <select name="" id="">
                            <option value="">Day</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                            <option value="">31</option>
                            </select>
                        <select name="" id="">
                            <option value="">Year</option>
                            <option value="">2005</option>
                            <option value="">2004</option>
                            <option value="">2003</option>
                            <option value="">2002</option>
                            <option value="">2001</option>
                            <option value="">2000</option>
                            <option value="">1999</option>
                            <option value="">1998</option>
                            <option value="">1997</option>
                            <option value="">1996</option>
                            <option value="">1995</option>
                            <option value="">1994</option>
                            <option value="">1993</option>
                            <option value="">1992</option>
                            <option value="">1991</option>
                            <option value="">1990</option>
                            <option value="">1989</option>
                            <option value="">1988</option>
                            <option value="">1987</option>
                            <option value="">1986</option>
                            <option value="">1985</option>
                            <option value="">1984</option>
                            <option value="">1983</option>
                            <option value="">1982</option>
                            <option value="">1981</option>
                            <option value="">1980</option>
                            </select>
                    </div>
                    <div className={style.selectBlock}>
                        <span>I am:</span><select name="" id="">
                            <option value="">Select Sex</option>
                            <option value="">Male</option>
                            <option value="">Female</option>
                            <option value="">Custom</option>
                        </select>
                    </div>
                    <span className={style.why}>By clicking Sign Up, you agree to our Terms. Learn how we collect, use and share your data in our Data Policy and how we use cookies and
                    similar technology in our Cookies Policy. You may receive SMS Notifications from us and can opt out any time.</span>
                    <div className={style.singUpBlockButton}>
                        <button type='submit'>Sing Up</button>
                    </div>
                </div>
                <div className={style.createPage}>
                    <span>Create a Page for celebrity, band or business.</span>
                </div>
            </div>
        </div>
    )
}

export default Login;