import React from 'react';
import style from './News.module.css'
import NewsSidePanel from './NewsSidePanel/SidePanel';
import ProfilePosts from './ProfilePosts/ProfilePosts';
import MainMenu from './MainMenu/MainMenu';


const News = ({ profile, posts, addPost, deletePost }) => {
    return (
        <div className={style.blockWrapper} >
            <div className={style.home}>
                <div className={style.contentBlock}>
                    <MainMenu profile={profile}/>
                    <ProfilePosts profile={profile} posts={posts} addPost={addPost} deletePost={deletePost}/>
                    <NewsSidePanel profile={profile} />
                </div>
            </div>
        </div>
    )
};

export default News;