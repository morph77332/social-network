import React from 'react';
import style from '../../../Profile/Timeline/SidePanel/SidePanel.module.css';
import { NavLink } from 'react-router-dom';
import UserMayKnow from './UserMauKnow/UserMayKnow';
import PreloaderSmall from '../../../../Common/Preloader/PreloaderSmall';

const PeopleYouMayKnow = ({users, follow, unFollow, progressAdd, isFetching}) => {
    return (
        <div className={style.sidePanelWrapper}>
            <div className={style.blockName}>
                <h4>People You May Know</h4>
                <NavLink to='/users'>See all</NavLink>
            </div>
            <div className={style.blockInfoItemUsers}>
                {isFetching
                    ? <PreloaderSmall />
                    : users.map(u => <UserMayKnow key={u.id} user={u} progressAdd={progressAdd} follow={follow} unFollow={unFollow} />)}
            </div>
        </div>
    )
}

export default PeopleYouMayKnow;