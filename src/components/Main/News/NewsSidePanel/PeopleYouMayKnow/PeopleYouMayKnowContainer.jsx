import React from 'react';
import { connect } from 'react-redux';
import { follow, unFollow, setUsers } from '../../../../../Redux/FriendsReducer';
import { usersAPI } from '../../../../../api/api';
import { useEffect } from 'react';
import PeopleYouMayKnow from './PeopleYouMayKnow';

const PeopleYouMayKnowContainer = ({ users, follow, unFollow, progressAdd, isFetching, setUsers }) => {

    useEffect(() => {
        usersAPI.getUsers(1, 3).then(response => {
            setUsers(response.items)
        });
    }, [0])

    return (
        <PeopleYouMayKnow users={users} follow={follow} unFollow={unFollow} progressAdd={progressAdd} isFetching={isFetching}/>
    )
}

const mapStateToProps = (state) => {
    return {
        progressAdd: state.friends.progressAdd,
        users: state.friends.users,
        isFetching: state.friends.isFetching
    }
}

export default connect(mapStateToProps, { follow, unFollow, setUsers })(PeopleYouMayKnowContainer);