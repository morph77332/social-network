import React from 'react';
import styleSide from '../../../Profile/Timeline/SidePanel/SidePanel.module.css';

let PhotosBlock = () => {
    return (
        <div className={styleSide.sidePanelWrapper}>
            <div className={styleSide.blockName}>
                <h4>Photos</h4>
                <a href='/home'>See all</a>
            </div>
            <div className={styleSide.blockInfo}>
                <div className={styleSide.photos}>
                    Some photos
                </div>
            </div>
        </div>
    )
}

export default PhotosBlock;