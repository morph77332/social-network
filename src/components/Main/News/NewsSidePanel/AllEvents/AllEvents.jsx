import React from 'react';
import style from '../../../Profile/Timeline/SidePanel/SidePanel.module.css';
import calendar from '../../../../../assets/images/calendar.png'

let CommunityBlock = () => {
    return (
        <div className={style.sidePanelWrapper}>
            <div className={style.blockInfoItemNews} >
                    <img src={calendar} alt='icon' />
                    <a href='/#'>1 event</a><span>this week</span>
                </div>
        </div>
    )
}

export default CommunityBlock;