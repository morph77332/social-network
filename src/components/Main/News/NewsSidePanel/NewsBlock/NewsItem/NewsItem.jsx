import React from 'react';
import style from '../../../../Profile/Timeline/SidePanel/SidePanel.module.css';
import news from '../../../../../../assets/images/news.png';
import { NavLink } from 'react-router-dom';

let NewsItem = (props) => {

    return (
        <div className={style.newsItem}>
            <img src={props.urlToImage ? props.urlToImage : news} alt='pic' />
            <a href={props.url}>{props.title} </a>
        </div>
    )
}

export default NewsItem;