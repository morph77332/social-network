import React, { useEffect } from 'react';
import NewsBlock from './NewsBlock';
import { connect } from 'react-redux';
import { getNews } from '../../../../../Redux/NewsReducer';

const NewsBlockContainer = ({ getNews, ...props }) => {

    useEffect(() => {
        getNews()
    }, [0])

    return (
        <NewsBlock news={props.news} />
    )
}

const mapStateToProps = (state) => {
    return {
        news: state.news.data
    }
}

export default connect(mapStateToProps, { getNews })(NewsBlockContainer);