import React from 'react';
import style from '../../../Profile/Timeline/SidePanel/SidePanel.module.css';
import PreloaderSmall from '../../../../Common/Preloader/PreloaderSmall';
import NewsItem from './NewsItem/NewsItem';
import settings from '../../../../../assets/images/more.png';
import { useState } from 'react';
import {showMenu} from '../../../../Common/ShowHideMenu/showMenu';

let Trending = (props) => {

    let [menu, setMenu] = useState(false);

    const news = props.news.slice(0, 3).map(n => <NewsItem key={n.source.id} urlToImage={n.urlToImage} title={n.title} url={n.url} />)
    let activeMenu = false;
    return (
        <div className={style.sidePanelWrapper}>
            <div className={style.blockName}>
                <h4>World News</h4>
                {menu ? <div className={style.newsPerseference}>
                    <div onMouseLeave={()=> setMenu(false)}>
                        <span>Settings</span>
                    </div>
                </div>
                : null}
                <button onClick={() => showMenu(menu, setMenu)} className={activeMenu ? style.moreButtonActive : style.moreButton}><img src={settings} alt='icon' /></button>
            </div>
            {props.news ?
                <div className={style.blockInfo}>
                    {news}
                </div>
                : <PreloaderSmall />}
            <span>Source: <a href={'https://newsapi.org'}>https://newsapi.org</a></span>
        </div>
    )
}

export default Trending;