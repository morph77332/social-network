import React from 'react';
import style from './SidePanel.module.css';
import NewsBlockContainer from './NewsBlock/NewsBlockContainer';
import AllEvents from './AllEvents/AllEvents';
// import PhotosBlock from './Photos/PhotosBlock';
import PeopleYouMayKnowContainer from './PeopleYouMayKnow/PeopleYouMayKnowContainer';
// import Footer from '../../../Footer/'


const SidePanel = (props) => {

    return (
        <div className={style.sidePanel}>
            <AllEvents />
            <NewsBlockContainer />
            <PeopleYouMayKnowContainer />
            {/* <PhotosBlock /> */}
            {/* <Footer /> */}
        </div>
    )
}

export default SidePanel;