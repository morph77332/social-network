import React from 'react';
import style from './MainMenu.module.css';
import { NavLink } from 'react-router-dom';
import user from '../../../../assets/images/user.png';
import newsfeed from '../../../../assets/images/newsfeed.png';
import messenger from '../../../../assets/images/messengerMenu.png';
import calendar from '../../../../assets/images/calendar.png';
import userPage from '../../../../assets/images/user_page.png';
import groups from '../../../../assets/images/groups.png';
import clock from '../../../../assets/images/clock.png';
import photos from '../../../../assets/images/photo_menu.png';
import user_star from '../../../../assets/images/user_page_star.png';

const MainMenu = ({ profile }) => {

    return (
        <div className={style.mainMenuWrapper}>
            <div className={style.mainMenuUser}>
                <div className={style.mainMenuUserAvatar}>
                    {profile
                        ? <img src={profile.photos ? profile.photos.small : user} alt='ava' />
                        : <img src={user} alt='ava' />}
                </div>
                <div className={style.mainMenuUserName}>
                    {profile ? <span>{profile.fullName}</span> : <span>User Name</span>}
                </div>
            </div>
            <div className={style.mainMenu}>
                <div className={style.mainMenuItem}>
                    <ul>
                        <li><img src={newsfeed} alt='icon' /><span>News Feed</span></li>
                        <li><img src={messenger} alt='icon' /><NavLink to={'/messenger'}>Messenger</NavLink></li>
                    </ul>
                </div>
                <div className={style.mainMenuItem}>
                    <h5>EXPLORE</h5>
                    <ul>
                        <li><img src={userPage} alt='icon' /><NavLink to={'/users'}>Pages</NavLink></li>
                        <li><img src={groups} alt='icon' /><span>Groups</span></li>
                        <li><img src={calendar} alt='icon' /><span>Events</span></li>
                        <li><img src={clock} alt='icon' /><span>On This Day</span></li>
                        <li><img src={photos} alt='icon' /><span>Photos</span></li>
                    </ul>
                </div>
                <div className={style.mainMenuItem}>
                    <h5>FRIENDS</h5>
                    <ul>
                        <li><img src={user_star} alt='icon' /><span>Choose Friends</span></li>
                    </ul>
                </div>
            </div>
        </div>
    )
};

export default MainMenu;