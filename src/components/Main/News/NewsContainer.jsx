import React from 'react';
import { addPost, deletePost } from '../../../Redux/ProfileReducer';
import { connect } from 'react-redux';
import News from './News';
import { Redirect } from 'react-router-dom';

const NewsContainer = ({ authUserProfile, posts, addPost, deletePost, isAuth }) => {
    return (
        isAuth
        ? <News profile={authUserProfile} posts={posts} addPost={addPost} deletePost={deletePost} />
        : <Redirect to={'/login'} />
    )
}

let mapStateToProps = (state) => {
    return {
        authUserProfile: state.auth.authUserProfile,
        isAuth: state.auth.isAuth,
        posts: state.main.posts,
    }
}

export default connect(mapStateToProps, { addPost, deletePost })(NewsContainer);