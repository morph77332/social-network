import React from 'react';
import style from '../../../Profile/Timeline/ProfilePosts/ProfilePosts.module.css';

const NewPostAddItem = ({ icon, name }) => {
    return (
        <div className={style.postMoreItem}>
            <img src={icon} alt='icon' />
            <span>{name}</span>
        </div>
    )
}

export default NewPostAddItem;

