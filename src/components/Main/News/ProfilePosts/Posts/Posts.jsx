import React, { useState } from 'react';
import style from '../Posts/Posts.module.css';
import more from '../../../../../assets/images/more.png'
import PostFeedBack from './PostFeedBack/PostFeedBack';
import PostMenu from './PostMenu/PostMenu';
import { showMenu } from '../../../../Common/ShowHideMenu/showMenu';

const Posts = (props ) => {

    let [activeMenu, setActiveMenu] = useState(false);
    const showPostMenu = () => {
        showMenu(activeMenu, setActiveMenu)
    }

    return (
        <div className={style.post}>
            <div className={style.postBlockWrapper}>
                <div className={style.postBlock}>
                    <div>
                        <img src='https://i.pinimg.com/736x/5d/81/0b/5d810bcb66a99e8b202c0b0b5db901e7.jpg' alt='avatar' />
                        <div className={style.postAuthor}>
                            <h5>Page</h5>
                            <span className={style.publicTime}>{props.time} hrs</span>
                        </div>
                    </div>
                    <PostMenu activeMenu={activeMenu} hideMenu={setActiveMenu} id={props.id} />
                    <button className={activeMenu ? style.editPostButtonActive : style.editPostButton} onClick={showPostMenu}><img src={more} alt='icon' /></button>
                </div>
            </div>
            <p>{props.text}</p>
            {props.image
                ? <div className={style.postPicture}>
                    <img src={props.image} alt='pic' />
                </div>
                : null}
            {/* <div className={style.likesCount}>
                <div>
                    <img src='https://i.pinimg.com/originals/39/44/6c/39446caa52f53369b92bc97253d2b2f1.png' alt='icon' />
                </div>
                <span>{props.likes}</span>
            </div> */}
            <PostFeedBack />
            {/* <div className={style.postComment}>
                <div className={style.commetAuthor}>
                    <img src='https://i.pinimg.com/736x/5d/81/0b/5d810bcb66a99e8b202c0b0b5db901e7.jpg' alt='avatar' />
                </div>
                <div className={style.commetText} >
                    <textarea placeholder='Write a comment..'></textarea>
                    <div>
                        <img src='https://cdn0.iconfinder.com/data/icons/text-editor-20/24/text-insert-emoji-512.png' alt='icon' />
                        <img src='https://sun1-93.userapi.com/gARepox9FJUYqjmWZpN2O5zgKwKAH4ZyPZz48g/oN_wbL8HglI.jpg' alt='icon' />
                    </div>
                </div>
            </div> */}
        </div>
    )
}

export default Posts;