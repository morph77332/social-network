import React from 'react';
import style from './PostMenu.module.css';

const PostMenu = ({ activeMenu, hideMenu, deletePost, id }) => {

    return (
        activeMenu
            ? <div className={style.postMenu} onMouseLeave={() => hideMenu(false)}>
                <div>
                    <span onClick={() => deletePost(id)}>Delete post</span>
                </div>
            </div>
            : null
    )
}

export default PostMenu;