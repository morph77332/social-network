import React from 'react';
import style from '../../Profile/Timeline/ProfilePosts/ProfilePosts.module.css';
import Posts from './Posts/Posts';
import { Field, reduxForm } from 'redux-form';
import { TextArea } from '../../../Common/FormsControls/FormsControls';
import pencil from '../../../../assets/images/pencil.png';
import camera from '../../../../assets/images/camera.png';
import liveCamera from '../../../../assets/images/live_camera.png';
import event from '../../../../assets/images/event.png';
import picture from '../../../../assets/images/picture.png';
import tagUser from '../../../../assets/images/tag_user.png';
import emoji from '../../../../assets/images/emoji.png';
import more from '../../../../assets/images/more.png';
import user from '../../../../assets/images/user.png';
import PostMenuItem from './PostMenu/PostMenu';
import NewPostAddItem from './PostMenu/NewPostAdd';

const PostForm = ({ handleSubmit, profile }) => {

    return (
        <form onSubmit={handleSubmit} className={style.newPostText}>
            <div className={style.newPostAuthor}><img src={profile ? profile.photos.small : user} alt='icon' /></div>
            <Field component={TextArea} name='newText' placeholder='Write something on this Page' />
            <button className={style.addPostButton} type='submit'>Post</button>
        </form>
    )
}

const PostReduxForm = reduxForm({ form: 'newPost' })(PostForm)

const NewsFeed = ({ deletePost, addPost, ...props }) => {

    const onSubmit = (values) => {
        addPost(values.newText)
        values.newText = ''
    }

    const post = [...props.posts].reverse().map(post => <Posts id={post.id} time={post.time} text={post.text} image={post.img} likes={post.likes} key={post.id} deletePost={deletePost} />)

    return (
        <div className={style.newsFeedContainer}>
            <div className={style.newPostContainer}>
                <div className={style.newPost}>
                    <PostMenuItem icon={pencil} name={'Create Post'} />
                    <PostMenuItem icon={camera} name={'Photo/Video'} />
                    <PostMenuItem icon={liveCamera} name={'Live Video'} />
                    <PostMenuItem icon={event} name={'Life Event'} />
                </div>
                <PostReduxForm onSubmit={onSubmit} profile={props.profile} />
                <div className={style.newPostAddMoreWrapper}>
                    <div className={style.newPostAddMore}>
                        <NewPostAddItem icon={picture} name={'photo/Video'} />
                        <NewPostAddItem icon={tagUser} name={'tag friends'} />
                        <NewPostAddItem icon={emoji} name={'feeling/activity'} />
                        <NewPostAddItem icon={more} name={''} />
                    </div>
                </div>
                <div className={style.addPostWrapper}>  {/* Styles for add post button */}
                </div>
            </div>
            {post}
        </div>
    )
}

export default NewsFeed;