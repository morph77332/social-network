import React from 'react';
import style from './Community.module.css'


const Community = () => {
    return (
        <div className={style.wrap}>
            <div className={style.community}>
                Community
            </div>
        </div>
    )
};

export default Community;