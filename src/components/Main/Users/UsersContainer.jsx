import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { addFriend, removeFriend, setCurrentPage, toggleProgressAdd, getUsersThunk, follow, unFollow } from '../../../Redux/FriendsReducer';
import Users from './Users';
import { AuthRedirect } from '../../../Hoc/AuthRedirect';
import { compose } from 'redux';

const UsersContainer = (props) => {

    useEffect(() => {
        const { currentPage, pageSize } = props
        props.getUsersThunk(currentPage, pageSize);
    }, [0])

    const onPageChanged = (pageNumber) => {
        const { pageSize } = props
        props.getUsersThunk(pageNumber, pageSize)
    }

    return (
        <Users {...props} onPageChanged={onPageChanged} />
    )
}

let mapStateToProps = (state) => {
    return {
        users: state.friends.users,
        pageSize: state.friends.pageSize,
        totalUsersCount: state.friends.totalUsersCount,
        currentPage: state.friends.currentPage,
        isFetching: state.friends.isFetching,
        progressAdd: state.friends.progressAdd,
    }
}

export default compose(connect(mapStateToProps, { addFriend, removeFriend, setCurrentPage, toggleProgressAdd, getUsersThunk, follow, unFollow }), AuthRedirect)(UsersContainer)

