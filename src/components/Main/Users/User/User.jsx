import React from 'react';
import style from '../Users.module.css'
import userPhoto from '../../../../assets/images/user.png'
import { NavLink } from 'react-router-dom';

const User = ({ user, progressAdd, follow, unFollow}) => {
    return (
                <div className={style.friendWrapper}>
                    <div className={style.userInfo}>
                        <div className={style.userAvatar}>
                            <NavLink to={'/profile/' + user.id}>
                                <img src={user.photos.small != null ? user.photos.small : userPhoto} alt='ava' />
                            </NavLink>
                        </div>
                        <div className={style.userAbout}>
                    <NavLink to={'/profile/' + user.id}><span>{user.name}</span></NavLink>
                            <p>{user.status}</p>
                            <p>{'user.job'}</p>
                            <p>{'user.location.city'}, {'user.location.country'}</p>
                        </div>
                    </div>
                    <div className={style.addFriend}>
                        {user.followed
                            ? <button disabled={progressAdd.some(id => id === user.id)} onClick={() => { unFollow(user.id) }}>Remove</button>
                            : <button disabled={progressAdd.some(id => id === user.id)} onClick={() => { follow(user.id) }}><img src='https://cdn0.iconfinder.com/data/icons/social-media-glyph-1/64/Facebook_Social_Media_User_Interface-35-512.png' alt='icon' /> Add Friend</button>}
                    </div>
                </div>
    )
}

export default User;