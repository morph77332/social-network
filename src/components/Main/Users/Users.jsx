import React from 'react';
import style from './Users.module.css'
import Contacts from './Contacts/Contacts';
import PreloaderSmall from '../../Common/Preloader/PreloaderSmall';
import Paginator from '../../Common/Paginator/Paginator';
import User from './User/User';

let Friends = ({ totalUsersCount, pageSize, currentPage, onPageChanged, ...props }) => {

    return (
        <div className={style.wrap}>
            <div className={style.friendsPageWrapper}>
                <div className={style.friendsWrapper}>
                    <div className={style.friendsRequest}>
                        <h4>No New Friend Requests</h4>
                        <a href="/home">View Sent Requests</a>
                    </div>
                    <div className={style.usersWrapper}>
                        <div className={style.mayKnow}>
                            <h4>People you may know</h4>
                        </div>
                        <div className={style.users}>
                            {props.isFetching
                                ? <><div className={style.preloaderWrapper}>
                                </div>
                                    <div className={style.preloaderUsers}><PreloaderSmall /></div></>
                                : null}
                            {props.users.map(u => <User key={u.id} user={u} progressAdd={props.progressAdd} follow={props.follow} unFollow={props.unFollow} />)}
                        </div>
                        <Paginator totalItemsCount={totalUsersCount} pageSize={pageSize} currentPage={currentPage} onPageChanged={onPageChanged} />
                    </div>
                </div>
                <Contacts />
            </div>
        </div>

    )
}

export default Friends;