import React from 'react';
import style from './Contacts.module.css'


const Contacts = () => {
    return (
        <div className={style.personalContacts}>
            <div className={style.addPC}>
                <h4>Add Personal Contacts</h4>
            </div>
            <div className={style.linkButtons}>
                <button><img src='https://i1.wp.com/www.appointedd.com/wp-content/uploads/2018/01/outlook-icon.png?ssl=1' alt='icon' /></button>
                <button><img src='https://upload.wikimedia.org/wikipedia/commons/4/4e/Gmail_Icon.png' alt='icon' /></button>
                <button><img src='https://seeklogo.net/wp-content/uploads/2016/06/YouTube-icon.png' alt='icon' /></button>
                <button><img src='https://cdn3.iconfinder.com/data/icons/2018-social-media-logotypes/1000/2018_social_media_popular_app_logo_reddit-512.png' alt='icon' /></button>
                <button><img src='https://i.pinimg.com/originals/a2/5f/4f/a25f4f58938bbe61357ebca42d23866f.png' alt='icon' /></button>
                <button><img src='https://cdn4.iconfinder.com/data/icons/social-media-flat-7/64/Social-media_Whatsapp-512.png' alt='icon' /></button>
            </div>
            <div className={style.chooseCommunicate}>
                <p>Chose how you communicate with friends.</p>
                <a href="/">See how it works</a> <p>or</p> <a href="/">manage imported contacts</a>
            </div>
            <div className={style.findFriend}>
                <button>Find Friends</button>
            </div>
        </div>
    )
}

export default Contacts;