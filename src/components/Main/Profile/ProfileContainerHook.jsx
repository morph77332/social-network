import React, { useEffect } from 'react';
import Profile from './Profile';
import { connect } from 'react-redux';
import { setUserProfile, getProfilethunk, getUserStatus, updateStatus, savePhoto, saveProfile } from '../../../Redux/ProfileReducer';
import { setAuthUserProfileThunk } from '../../../Redux/AuthReducer';
import { withRouter } from 'react-router-dom';
import PreloaderSmall from '../../Common/Preloader/PreloaderSmall';
import { compose } from 'redux';
import style from './Profile.module.css'

const ProfileContainerHook = ({ profile, status, updateStatus, savePhoto, preloaderPhoto, aboutEditMode, saveProfile, authUserId, ...props }) => {

    let userId = props.match.params.userId;
    if (!userId) {
        userId = props.authUserId
    };
    useEffect(() => {
        props.getProfilethunk(userId);
        props.getUserStatus(userId);
    }, [userId]);

    return (
        profile
            ? <Profile profile={profile} authUserId={authUserId} status={status} updateStatus={updateStatus} savePhoto={savePhoto} preloaderPhoto={preloaderPhoto} aboutEditMode={aboutEditMode} saveProfile={saveProfile} onchange={onchange} />
            : <div className={style.preloaderProfilePage}><PreloaderSmall /></div>
    )
}

let mapStateToProps = (state) => ({
    profile: state.main.profile,
    status: state.main.status,
    authUserId: state.auth.userId,
    isAuth: state.auth.isAuth,
    preloaderPhoto: state.main.preloader,
    aboutEditMode: state.main.editMode
})

export default compose(connect(mapStateToProps, { setUserProfile, getProfilethunk, setAuthUserProfileThunk, getUserStatus, updateStatus, savePhoto, saveProfile, }), withRouter)(ProfileContainerHook)