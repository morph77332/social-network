import React from 'react';
import profileStyle from './Profile.module.css';
import ProfileInfoContainer from './ProfileInfo/ProfileInfoContainer';
import { Route } from 'react-router-dom';
import Photos from './Photos/Photos';
import About from './About/About';
import Timeline from './Timeline/Timeline';
import FriendsContainer from './Friends/FriendsContainer';

const Profile = ({ profile, status, updateStatus, savePhoto, preloaderPhoto, aboutEditMode, saveProfile, onchange, authUserId }) => {

    return (
        <div className={profileStyle.blockWrapper} >
            <div className={profileStyle.home}>
                <ProfileInfoContainer profile={profile} status={status} updateStatus={updateStatus} savePhoto={savePhoto} preloaderPhoto={preloaderPhoto} />
                <div className={profileStyle.contentBlock}>
                    <Route exact path={`/profile`} render={() => <Timeline profile={profile} />} />
                    <Route exact path={`/profile/${profile.userId}`} render={() => <Timeline profile={profile} />} />
                    <Route exact path={`/profile/${profile.userId}/timeline`} render={() => <Timeline profile={profile} />} />
                    <Route exact path={`/profile/${profile.userId}/about`} render={() => <About aboutEditMode={aboutEditMode} authUserId={authUserId} saveProfile={saveProfile} profile={profile} onchange={onchange} />} />
                    <Route exact path={`/profile/${profile.userId}/friends`} render={() => <FriendsContainer />} />
                    <Route exact path={`/profile/${profile.userId}/photos`} render={() => <Photos />} />
                </div>
            </div>
        </div>
    )
};

export default Profile;
