import React from 'react';
import style from './ProfileMenu.module.css'
import { NavLink } from 'react-router-dom/cjs/react-router-dom.min';
import { withRouter } from 'react-router-dom';

const ProfileMenu = ({profile, ...props}) => {

    let path = (menuPath) => `/profile/${profile.userId}/${menuPath}`;
    const menuItem = (menuPath, menuName) => <li className = { path(menuPath) === props.location.pathname ? style.selectedMenu : null}><NavLink to={path(menuPath)}>{menuName}</NavLink></li>;

    return (
        <div className={style.profileNav}>
            <nav>
                <ul>
                    {menuItem('timeline', 'Timeline')}
                    {menuItem('about', 'About')}
                    {menuItem('friends', 'Friends')}
                    {menuItem('photos', 'Photos')}
                    {menuItem('more', 'More')}
                </ul>
            </nav>
        </div>
    )
}

export default withRouter(ProfileMenu);