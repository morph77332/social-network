import React from 'react';

const ProfileButtons = () => {
    return (
        <div>
            <button><img src='https://cdn4.iconfinder.com/data/icons/mixed-2-1/106/Untitled-1277-512.png' alt='icon' />Like</button>
            <button><img src='https://images.vexels.com/media/users/3/131979/isolated/preview/738e536059f8ca89ae33a97f0edca2ff-rss-feed-icon-by-vexels.png' alt='icon' />Follow</button>
            <button><img src='https://www.freeiconspng.com/uploads/black-facebook-messenger-logo-29.png' alt='icon' />Message</button>
            <button><img src='https://cdn2.iconfinder.com/data/icons/navigation-17/24/navigation-show-more-horizontal-512.png' alt='icon' /></button>
        </div>
    )
}

export default ProfileButtons;