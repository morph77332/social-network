import React from 'react';
import pencil from '../../../../../assets/images/pencil.png';
import list from '../../../../../assets/images/list.png';
import more from '../../../../../assets/images/more.png';

const MainProfileButtons = ({ setAboutEditMode, aboutEdit }) => {
    return (
        <div>
            {aboutEdit
                ? <button onClick={() => setAboutEditMode(false)}><img src={pencil} alt='icon' />Save</button>
            : <button onClick={() => setAboutEditMode(true)}><img src={pencil} alt='icon' />Edit Profile</button>}
            <button><img src={list} alt='icon' />Activity Log</button>
            <button><img src={more} alt='icon' /></button>
        </div>
    )
}

export default MainProfileButtons;