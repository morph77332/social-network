import React, { useState, useEffect } from 'react';
import ProfileInfo from './ProfileInfo';
import { connect } from 'react-redux';
import { saveProfile, setAboutEditMode } from '../../../../Redux/ProfileReducer';



let ProfileInfoContainer = ({ profile, status, userId, updateStatus, savePhoto, preloaderPhoto, setAboutEditMode, saveProfile, aboutEdit}) => {
    let [editMode, setEditMode] = useState(false);
    let [neWstatus, setStatus] = useState(status);

    useEffect(() => {
        setStatus(status);
    }, [status]);

    const activeEditMode = () => {
        setEditMode(true);
        if (profile.userId !== userId) {
            return setEditMode(false);
        }
    };
    const deActiveEditMode = () => {
        setEditMode(false);
        updateStatus(neWstatus);
    };

    const onStatusChange = (e) => {
        setStatus(e.target.value)
    };

    const onPhotoLargeSelected = (e) => {
        savePhoto(e.target.files[0]);
    }

    return (
        <ProfileInfo profile={profile} activeEditMode={activeEditMode} deActiveEditMode={deActiveEditMode} neWstatus={neWstatus}
            onStatusChange={onStatusChange} onPhotoLargeSelected={onPhotoLargeSelected} status={status} userId={userId} saveProfile={saveProfile}
            updateStatus={updateStatus} savePhoto={savePhoto} preloaderPhoto={preloaderPhoto} editMode={editMode} aboutEdit={aboutEdit} setAboutEditMode={setAboutEditMode}/>
    )
}

let mapStateToProps = (state) => {
    return {
        userId: state.auth.userId,
        aboutEdit: state.main.editMode,
    }
}

export default connect(mapStateToProps, { setAboutEditMode, saveProfile })(ProfileInfoContainer)