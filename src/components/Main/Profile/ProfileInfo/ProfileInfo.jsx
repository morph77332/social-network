import React from 'react';
import style from './ProfileInfo.module.css'
import photo from './../../../../assets/images/user.png';
import profilePhoto from './../../../../assets/images/photo.png'
import camera from '../../../../assets/images/camera.png';
import PreloaderSmall from '../../../Common/Preloader/PreloaderSmall';
import ProfileMenu from './ProfileMenu/ProfileMenu';
import MainProfileButtons from './ProfileButtons/MainProfileButton';
import ProfileButtons from './ProfileButtons/ProfileButtons';


let ProfileInfo = ({ profile, userId, preloaderPhoto, onPhotoLargeSelected, activeEditMode, deActiveEditMode, onStatusChange, editMode, neWstatus, setAboutEditMode, aboutEdit}) => {

    return (
        <div className={style.profilePreview}>
            <div className={style.profileImage}>
                {profile.userId === userId &&
                    <div className={style.upLoadPhoto}>
                        {preloaderPhoto ? <PreloaderSmall /> : <><input type='file' onChange={onPhotoLargeSelected} title='' /> <img src={camera} alt='icon' /></>}
                    </div>}
                <div className={style.largePhoto}>
                    <img src={profile.photos.large ? profile.photos.large : profilePhoto} alt="profilePhoto" />
                </div>
                <div className={style.profileInfoWrapper}>
                    <div className={style.profileAvatar}>
                        {profile.photos.small ? <img src={profile.photos.small} alt="pic" /> : <img className={style.photo} src={photo} alt='pic' />}
                    </div>
                    <div className={style.profileInfo}>
                        <div className={style.profileName}>
                            <h2>{profile.fullName}</h2>
                            {editMode
                                ? <span><input autoFocus={true} onBlur={deActiveEditMode} onChange={onStatusChange} value={neWstatus} placeholder='Status should be here' /></span>
                                : <div onClick={activeEditMode}>
                                    {neWstatus ? <span>{neWstatus}</span> : <input disabled />}
                                </div>}
                        </div>
                        {profile.userId === userId && <MainProfileButtons setAboutEditMode={setAboutEditMode} aboutEdit={aboutEdit}/>}
                        {profile.userId !== userId && <ProfileButtons />}
                    </div>
                </div>
            </div>
            <ProfileMenu profile={profile} />
        </div>
    )
}

export default ProfileInfo;