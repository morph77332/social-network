import React from 'react';
import style from './AboutItem.module.css';

const AboutItem = () => {
    return (
        <div className={style.aboutInfoItem}>
            <div className={style.aboutInfoItemImg}>
                <img src='' alt='icon' />
            </div>
            <div className={style.aboutInfoItemDes}>
                <span>Some info at <a href='/#'>some link</a></span>
                <br /><span>Data</span>
            </div>
        </div>
    )
}

export default AboutItem;

