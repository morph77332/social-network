import React from 'react';
import style from './AboutItem.module.css';
import { Field } from 'redux-form';

const AboutItem2 = (props) => {
    return (
        <div className={style.aboutInfoBlock2Item}>
            <div className={style.aboutInfoBlock2ItemImg}>
                <img src={props.img} alt='icon' />
            </div>
            {props.editMode
                ? <Field component={'input'} name={props.name} placeholder={props.placeholder} className={style.input} />
                : <Field component={'input'} name={props.name} placeholder={props.placeholder} disabled />}
        </div>
    )
}

export default AboutItem2;

