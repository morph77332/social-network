import React from 'react';
import style from './About.module.css';
import user from '../../../../assets/images/user.png';
import job from '../../../../assets/images/social/job_grey.png';
import info from '../../../../assets/images/social/info.png';
import AboutItem from './AboutItem/AboutItem';
import AboutItem2 from './AboutItem/AboutItem2';
import facebook from '../../../../assets/images/social/facebook_grey.png';
import twitter from '../../../../assets/images/social/twitter_grey.png';
import instagram from '../../../../assets/images/social/instagram_grey.png';
import vk from '../../../../assets/images/social/vk_grey.png';
import youtube from '../../../../assets/images/social/youtube_grey.png';
import git from '../../../../assets/images/social/github_grey.png';
import { reduxForm } from 'redux-form';

const AboutForm = ({ handleSubmit, aboutEditMode, authUserId, userId}) => {

    return (
        <form onSubmit={handleSubmit} className={style.aboutInfoBlock2}>
            <AboutItem2 img={user} name={'fullName'} placeholder={'Full Name'}  editMode={aboutEditMode} />
            <AboutItem2 img={job} name={'lookingForAJobDescription'} placeholder={'Looking For A Job Description'} editMode={aboutEditMode} />
            <AboutItem2 img={info} name={'aboutMe'} placeholder={'About Me'}  editMode={aboutEditMode} />
            <AboutItem2 img={facebook} name={'contacts.facebook'} placeholder={'facebook.com'}  editMode={aboutEditMode} />
            <AboutItem2 img={twitter} name={'contacts.twitter'} placeholder={'twitter.com'}  editMode={aboutEditMode} />
            <AboutItem2 img={instagram} name={'contacts.instagram'} placeholder={'instagram.com'}  editMode={aboutEditMode} />
            <AboutItem2 img={vk} name={'contacts.vk'} placeholder={'vk.com'}  editMode={aboutEditMode} />
            <AboutItem2 img={youtube} name={'contacts.youtube'} placeholder={'youtube.com'}  editMode={aboutEditMode} />
            <AboutItem2 img={git} name={'contacts.github'} placeholder={'github.com'}  editMode={aboutEditMode} />
            {userId === authUserId ? <button type='submit'>send</button> : null}
        </form>
    )
}

const AboutReduxForm = reduxForm({ form: 'newInfo' })(AboutForm)

const About = ({ aboutEditMode, saveProfile, profile, authUserId }) => {

    let onSubmit = (formData) => {
        saveProfile(formData)
    }

    return (
        <div className={style.aboutWrapper}>
            <div className={style.aboutHeader}>
                <div className={style.aboutHeaderIcon}>
                    <img src={user} alt='icon' />
                </div>
                <h3>About</h3>
            </div>
            <div className={style.about}>
                <div className={style.aboutNav}>
                    <ul>
                        <li>Overview</li>
                        <li>Work and Education</li>
                        <li>Places You've Lived</li>
                        <li>Contact and Basic Info</li>
                        <li>Family and Relationships</li>
                        <li>Details About You</li>
                        <li>Life Events</li>
                    </ul>
                </div>
                <div className={style.aboutInfoBlock}>
                    <AboutItem />
                    <AboutItem />
                    <AboutItem />
                    <AboutItem />
                    <AboutItem />
                </div>
                <AboutReduxForm initialValues={profile} authUserId={authUserId} userId={profile.userId} onSubmit={onSubmit} aboutEditMode={aboutEditMode} />
            </div>
        </div>

    )
};

export default About;