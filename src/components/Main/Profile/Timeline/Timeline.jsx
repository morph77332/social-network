import React from 'react';
import SidePanel from '../Timeline/SidePanel/SidePanel';
import ProfilePostsContainer from '../Timeline/ProfilePosts/ProfilePostsContainer';

const Timeline = ({profile}) => {
    return (
        <>
            <SidePanel profile={profile}/>
            <ProfilePostsContainer profile={profile}/>
        </>
    )
}

export default Timeline;