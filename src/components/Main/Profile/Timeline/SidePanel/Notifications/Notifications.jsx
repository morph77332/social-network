import React from 'react';
import styleStatus from './Notifications.module.css';

const Notifications = () => {

    return (
        <div className={styleStatus.sidePanelWrapper}>
            <span>{'Some notifications'}</span>
        </div>
    )
}

export default Notifications;
