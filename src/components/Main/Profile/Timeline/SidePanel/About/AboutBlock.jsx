import React from 'react';
import styleSide from '../SidePanel.module.css';
import aboutMe from '../../../../../../assets/images/aboutMe.png';
import facebook from '../../../../../../assets/images/social/facebook.png';
import twitter from '../../../../../../assets/images/social/twitter.png';
import instagram from '../../../../../../assets/images/social/instagram.png';
import vk from '../../../../../../assets/images/social/vk.png';
import github from '../../../../../../assets/images/social/github.png';
import job from '../../../../../../assets/images/social/job.png';
import about from '../../../../../../assets/images/about.png';

let AboutBlock = ({ profile }) => {

    const social = (name, info, icon) => {
        if (info) {
            return (
                <div className={styleSide.blockInfoItem}>
                    {info
                        ? <><div className={styleSide.blockInfoItemImg}><img src={icon} alt='icon' /></div>
                            <span>{name}: {info}</span></>
                        : null}
                </div>
            )
        } else {
            return null
        }
    }

    return (
        <div className={styleSide.sidePanelWrapper}>
            <div className={styleSide.blockNameAbout}>
                <div><img src={about} alt='icon' /><h5>Intro</h5></div>
            </div>
            <div className={styleSide.blockInfo}>
                <div className={styleSide.blockInfoItem}>
                    <div className={styleSide.blockInfoItemImg}><img src={aboutMe} alt='icon' /></div>
                    {profile.aboutMe
                        ? <span>About Me: {profile.aboutMe}</span>
                        : <span>About Me: {'about me...'}</span>}
                </div>
                {social('Looking for a job', profile.lookingForAJobDescription, job)}
                {social('Facebook', profile.contacts.facebook, facebook)}
                {social('Twitter', profile.contacts.twitter, twitter)}
                {social('Instagram', profile.contacts.instagram, instagram)}
                {social('VK', profile.contacts.vk, vk)}
                {social('GitHub', profile.contacts.github, github)}
            </div>
        </div>
    )
}

export default AboutBlock;