import React from 'react';
import styleSide from '../SidePanel.module.css';
import friends from '../../../../../../assets/images/friends_small.png';

let ProfileFriends = () => {
    return (
        <div className={styleSide.sidePanelWrapper}>
            <div className={styleSide.blockName}>
                <div><img src={friends} alt='icon' /><h5>Friends</h5></div>
                <a href='/home'>See all</a>
            </div>
            <div className={styleSide.blockInfo}>
                <div className={styleSide.photos}>
                    <div><img src='https://api.time.com/wp-content/uploads/2019/08/better-smartphone-photos.jpg?quality=85&w=1024&h=628&crop=1' alt='pic' /></div>
                    <div><img src='https://s22246.pcdn.co/wp-content/uploads/2019/11/0-Mauritius-Beachcomber-treux-au-biches-11-1024x683.jpg' alt='pic' /></div>
                    <div><img src='https://media-cdn.tripadvisor.com/media/photo-s/07/1a/82/32/scootr-icture.jpg' alt='pic' /></div>
                </div>
            </div>
        </div>
    )
}

export default ProfileFriends;