import React from 'react';
import styleSide from './SidePanel.module.css';
import AboutBlock from './About/AboutBlock';
import ProfileFriends from './ProfileFriends/ProfileFriends';
import PhotosBlock from './Photos/PhotosBlock';
import Notifications from './Notifications/Notifications';


const SidePanel = ({profile}) => {

    return (
        <div className={styleSide.sidePanel}>
            <Notifications />
            <AboutBlock profile={profile}/>
            <PhotosBlock />
            <ProfileFriends/>
        </div>
    )
}

export default SidePanel;