import React from 'react';
import style from './PageLiked.module.css';

let PageLiked = () => {
    return (
        <div className={style.sidePanelWrapper}>
            <div className={style.blockName}>
                <h4>Page liked by this page</h4>
                <a href='/home'>See all</a>
            </div>
            <div className={style.blockInfo}>
                <div className={style.likeItemContainer}>
                    <div className={style.likeItem}>
                        <div><img src='https://i.pinimg.com/736x/5d/81/0b/5d810bcb66a99e8b202c0b0b5db901e7.jpg' alt='avatar' /></div>
                        <div className={style.likeItemAuthor}>
                            <h4>Page Title</h4>
                        </div>
                    </div>
                    <button><img src='https://cdn2.iconfinder.com/data/icons/hand-drawn-2/100/V-512.png' alt='icon' /> Liked</button>
                </div>
                <div className={style.likeItemContainer}>
                    <div className={style.likeItem}>
                        <div><img src='https://i.pinimg.com/736x/5d/81/0b/5d810bcb66a99e8b202c0b0b5db901e7.jpg' alt='avatar' /></div>
                        <div className={style.likeItemAuthor}>
                            <h4>Page Title</h4>
                        </div>
                    </div>
                    <button><img src='https://cdn2.iconfinder.com/data/icons/hand-drawn-2/100/V-512.png' alt='icon' /> Liked</button>
                </div>
                <div className={style.likeItemContainer}>
                    <div className={style.likeItem}>
                        <div><img src='https://i.pinimg.com/736x/5d/81/0b/5d810bcb66a99e8b202c0b0b5db901e7.jpg' alt='avatar' /></div>
                        <div className={style.likeItemAuthor}>
                            <h4>Page Title</h4>
                        </div>
                    </div>
                    {true
                        ? <button><img src='https://cdn2.iconfinder.com/data/icons/hand-drawn-2/100/V-512.png' alt='icon' /> Liked</button>
                        : <button>Like!</button>}
                </div>
            </div>
        </div>
    )
}

export default PageLiked;