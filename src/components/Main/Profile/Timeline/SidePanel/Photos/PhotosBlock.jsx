import React from 'react';
import styleSide from '../SidePanel.module.css';
import picture from '../../../../../../assets/images/photos.png'

let PhotosBlock = () => {
    return (
        <div className={styleSide.sidePanelWrapper}>
            <div className={styleSide.blockName}>
                <div><img src={picture} alt='icon' /><h5>Photos</h5></div>
                <a href='/home'>See all</a>
            </div>
            <div className={styleSide.blockInfo}>
                <div className={styleSide.photos}>
                    <div><img src='https://api.time.com/wp-content/uploads/2019/08/better-smartphone-photos.jpg?quality=85&w=1024&h=628&crop=1' alt='pic' /></div>
                    <div><img src='https://s22246.pcdn.co/wp-content/uploads/2019/11/0-Mauritius-Beachcomber-treux-au-biches-11-1024x683.jpg' alt='pic' /></div>
                    <div><img src='https://media-cdn.tripadvisor.com/media/photo-s/07/1a/82/32/scootr-icture.jpg' alt='pic' /></div>
                    <div><img src='https://dynaimage.cdn.cnn.com/cnn/q_auto,h_600/https%3A%2F%2Fcdn.cnn.com%2Fcnnnext%2Fdam%2Fassets%2F191226173335-02c-week-in-photos-1226.jpg' alt='pic' /></div>
                    <div><img src='https://cdn.shopify.com/s/files/1/0994/0236/articles/jack-russell-terrier.JPG?v=1505503208' alt='pic' /></div>
                    <div><img src='https://images.news18.com/ibnlive/uploads/2019/08/offbeat-photos-fire-breathing.jpg' alt='pic' /></div>
                    <div><img src='https://s2.best-wallpaper.net/wallpaper/1920x1200/1808/Red-poppies-flowers-sun-rays_1920x1200.jpg' alt='pic' /></div>
                    <div><img src='https://cnet2.cbsistatic.com/img/2CY6-JngeLdnu-5bemgYGADEv2s=/2300x1293/2019/10/09/7b3bf734-3375-4e4d-8e2f-c6c6103c56c4/moment-photo-0cb4f4b3.jpg' alt='pic' /></div>
                    <div><img src='https://clicklikethis.com/wp-content/uploads/2016/11/gopro-photography-tips.jpg' alt='pic' /></div>
                </div>
            </div>
        </div>
    )
}

export default PhotosBlock;