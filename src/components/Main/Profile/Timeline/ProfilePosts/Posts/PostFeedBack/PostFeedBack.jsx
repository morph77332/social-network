import React from 'react';
import style from './PostFeedBack.module.css';
import { connect } from 'react-redux';

const PostFeedBack = (props) => {
    return (
        <div className={style.postFeedback}>
            <button onClick={props.like}>
                <img src='https://images.vexels.com/media/users/3/157338/isolated/preview/4952c5bde17896bea3e8c16524cd5485-facebook-like-icon-by-vexels.png' alt='icon' />
                <span>Like</span>
            </button>
            <button>
                <img src='https://cdn2.iconfinder.com/data/icons/online-communication-i/24/Material_icons-04-33-512.png' alt='icon' />
                <span>Comment</span>
            </button>
            <button>
                <img src='https://cdn1.iconfinder.com/data/icons/arrows-i/24/Material_icons-02-08-512.png' alt='icon' />
                <span>Share</span>
            </button>
        </div>
    )
}


export default connect(null, { })(PostFeedBack);