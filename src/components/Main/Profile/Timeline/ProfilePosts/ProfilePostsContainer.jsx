import React from 'react';
import { addPost, deletePost } from '../../../../../Redux/ProfileReducer';
import {connect} from 'react-redux';
import NewsFeed from './ProfilePosts';


const ProfilePostContainer = ({ authUserProfile, posts, addPost, deletePost }) => {
    return (
        <NewsFeed profile={authUserProfile} posts={posts} addPost={addPost} deletePost={deletePost} />
    )
}

let mapStateToProps = (state) => {
    return {
        posts: state.main.posts,
        authUserProfile: state.auth.authUserProfile
    }
}

export default connect(mapStateToProps, { addPost, deletePost })(ProfilePostContainer);