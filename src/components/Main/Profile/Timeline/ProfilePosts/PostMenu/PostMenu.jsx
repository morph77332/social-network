import React from 'react';
import style from '../ProfilePosts.module.css';

const PostMenuItem = ({icon, name}) => {
    return (
        <div className={style.newPostItem}>
            <img src={icon} alt='icon' />
            <span>{name}</span>
        </div>
    )
}

export default PostMenuItem;

