import React from 'react';
import style from './DialogItem.module.css'
import { NavLink } from 'react-router-dom';

const DialogItem = (props) => {

    let path = '/messenger/' + props.id;

    return (
            <NavLink to={path} activeClassName={style.dialogItemActive}>
                <div className={style.dialogItem}>
                    <div className={style.dialogItemInfoContainer}>
                        <div className={style.dialogItemInfo}>
                            <div className={style.dialogItemAvatar} >
                                <img src={props.avatar} alt='ava' />
                            </div>
                            <div className={style.dialogItemMessage}>
                                <h5>{props.name}</h5>
                                <p>{props.text}</p>
                            </div>
                        </div>
                        <div className={style.dialogTime}>
                            <p>{props.time}</p>
                        </div>
                    </div>
                </div>
            </NavLink >
    )
}

export default DialogItem;