import React from 'react';
import style from './Messenger.module.css'
import DialogContainer from './Dialog/DialogContainer';
import DialogItem from './DialogItem/DialogItem';

const Messenger = ({dialogs}) => {
   
    let dialogItem = dialogs.map(d => <DialogItem id={d.id} name={d.name} text={d.text} avatar={d.avatar} time={d.time} key={d.id} />);
    return (
        <div className={style.wrap}>
            <div className={style.messenger}>
                <div className={style.messengerWrapper}>
                    <div className={style.messengerSidebar}>
                        <div className={style.messengerHeader}>
                            <div>
                                <button><img src='https://cdn4.iconfinder.com/data/icons/ui-standard/96/Setting-512.png' alt='icon' /></button>
                            </div>
                            <div>
                                <h5>Messenger</h5>
                            </div>
                            <div>
                                <button><img src='https://getdrawings.com/free-icon/note-icon-png-59.png' alt='icon' /></button>
                            </div>
                        </div>
                        <div className={style.dialogsPanel}>
                            <div className={style.searchMessanger}><textarea placeholder='Search Messenger' /></div>
                            <div className={style.dialogs}>
                                {dialogItem}
                            </div>
                        </div>
                    </div>
                    <div className={style.messengerMain}>
                        <div className={style.messengerMainHeader} >
                            <div className={style.messengerMainHeaderBlock}>
                                <h5>Dialogs</h5>
                                <button><img src='https://getdrawings.com/free-icon/telephone-icon-png-69.png' alt='icon' /></button>
                                <button><img src='https://webstockreview.net/images/photographer-clipart-lights-camera-14.png' alt='icon' /></button>
                                <button><img src='https://pbs.twimg.com/profile_images/2206300544/infonic-transparent_500x500.png' alt='icon' /></button>
                            </div>
                        </div>
                        <DialogContainer />
                    </div>
                </div>
            </div>
        </div>

    )
};

export default Messenger;
