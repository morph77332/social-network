import React from 'react';
import style from './FriendInfo.module.css'


let FriendInfo = (props) => {
    return (
        <div className={style.friendWrapper}>
            <div className={style.friendPhoto}>
                <img src={props.avatar} alt='ava' />
            </div>
            <div className={style.friendInfo}>
                <h5>{props.name}</h5>
                <p>You're friends on Facebook</p>
                <p>{props.job}</p>
                <p>Lives in {props.city}</p>
            </div>
        </div >
    )
}

export default FriendInfo;