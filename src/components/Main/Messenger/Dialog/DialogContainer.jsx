
import { sendMessageActionCreator } from './../../../../Redux/MessengerReducer';
import Dialog from './Dialog';
import { connect } from 'react-redux';

let mapStateToProps = (state) => {

    return {
        messenger: state.messenger,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        sendNewMessage: (text) => {
            dispatch(sendMessageActionCreator(text))
        }
    }
}

const DialogContainer = connect(mapStateToProps, mapDispatchToProps)(Dialog)

export default DialogContainer;
