import React from 'react';
import style from './Dialog.module.css'
import Message from './Message/Message';
import FriendInfo from './FrinedInfo/FriendInfo';
import { Route } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { TextArea } from '../../../Common/FormsControls/FormsControls';
import { required } from '../../../../utils/validators/validators';


const DialogForm = ({ handleSubmit }) => {
    return (
        <form onSubmit={handleSubmit} className={style.newMessageTextArea}>
            <Field component={TextArea} validate={[required]} name='textMessage' placeholder='Type a message...' />
            <button type='submit'>send</button>
        </form>
    )
}

const MessageReduxForm = reduxForm({ form: 'newMessage' })(DialogForm)

const Dialog = ({ messenger, sendNewMessage}) => {

    let messagesElement = messenger.messages.map(m => <Message message={m.text} img={m.img} key={m.id} />)
    let friendInfo = messenger.friendInfo.map(f => <FriendInfo id={f.id} name={f.name} avatar={f.avatar} job={f.job} city={f.city} />);

    let onSubmit = (values) => {
        sendNewMessage(values.textMessage);
    }

        return (
            <div className={style.conatinerMessage}>
                <div className={style.messengerMainFriend}>
                    <Route path={'/messenger/' + 1} render={() => friendInfo[0]} />
                    <Route path={'/messenger/' + 2} render={() => friendInfo[1]} />
                    <Route path={'/messenger/' + 3} render={() => friendInfo[2]} />
                    <Route path={'/messenger/' + 4} render={() => friendInfo[3]} />
                    <Route path={'/messenger/' + 5} render={() => friendInfo[4]} />
                </div>
                <div className={style.messages}>
                    <div className={style.messengeTime}>
                        <p>TUE 5:28PM</p>
                    </div>
                    {/* <div className={style.messageItem}>
                    <div className={style.messageItemPhoto}>
                        <img src='https://i.pinimg.com/736x/64/a8/8f/64a88f80d6b5a43b58d14c20c7ef4b89.jpg' alt='ava' />
                    </div>
                    <div className={style.messageTextReceived}>
                        <p>Hi!</p>
                    </div>
                </div> */}
                    <Route path={'/messenger/' + 1} render={() => messagesElement[0]} />
                    <Route path={'/messenger/' + 2} render={() => messagesElement[1]} />
                    <Route path={'/messenger/' + 3} render={() => messagesElement[2]} />
                    <Route path={'/messenger/' + 4} render={() => messagesElement[3]} />
                    <Route path={'/messenger/' + 5} render={() => messagesElement[4]} />

                </div>
                <div className={style.newMessageBlock}>
                    <div className={style.newMessageText}>
                        <MessageReduxForm onSubmit={onSubmit} />
                    </div>
                </div>
            </div>
        )
}

export default Dialog;
