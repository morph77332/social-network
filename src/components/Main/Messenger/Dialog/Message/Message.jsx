import React from 'react';
import style from './Message.module.css'

let Message = (props) => {
    return (
        <div className={style.messageItemSended}>
            <div className={style.messageTextSended}>
                <p>{props.message}</p>
            </div>
            <div className={style.messageItemPhoto}>
                <img src={props.img} alt='ava' />
            </div>
        </div>
    )
}

export default Message;