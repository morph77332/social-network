import React from 'react';
import { connect } from 'react-redux';
import Messenger from './Messenger';
import { AuthRedirect } from '../../../Hoc/AuthRedirect';
import { compose } from 'redux';

class MessengerContainer extends React.Component {
    render() {
        return (
            <Messenger {...this.props}/>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        dialogs: state.messenger.dialogs,
    }
}

export default compose(connect(mapStateToProps, {}),AuthRedirect)(MessengerContainer)


