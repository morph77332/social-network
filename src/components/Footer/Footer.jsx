import React from 'react';
import style from './Footer.module.css';

const Footer = () => {
    return (
        <div className={style.footerWrapper}>
            <div className={style.footer}>
                <div className={style.lang}>
                    <ul>
                        <li><a href='home/'>Русский</a></li>
                        <li><a href='home/'>English</a></li>
                        <li><a href='home/'>Čeština</a></li>
                        <li><a href='home/'>Slovenčina</a></li>
                        <li><a href='home/'>Tiếng Việt</a></li>
                        <li><a href='home/'>Deutsch</a></li>
                        <li><a href='home/'>Français (France)</a></li>
                        <li><a href='home/'>Български</a></li>
                        <li><a href='home/'>Polski</a></li>
                        <li><a href='home/'>Español</a></li>
                        <li><a href='home/'>Português (Brasil)</a></li>
                        <button><img src='https://cdn2.iconfinder.com/data/icons/navigation-17/24/navigation-show-more-horizontal-512.png' alt='' /></button>
                    </ul>
                </div>
                <div className={style.links}>
                    <ul>
                        <li><a href='home/'>Sign Up</a></li>
                        <li><a href='home/'>Log in</a></li>
                        <li><a href='home/'>Messenger</a></li>
                        <li><a href='home/'>Facebook Lite</a></li>
                        <li><a href='home/'>Watch</a></li>
                        <li><a href='home/'>People</a></li>
                        <li><a href='home/'>Pages</a></li>
                        <li><a href='home/'>Page Categories</a></li>
                        <li><a href='home/'>Places</a></li>
                        <li><a href='home/'>Games</a></li>
                        <li><a href='home/'>Locations</a></li>
                        <li><a href='home/'>Marketplace</a></li>
                        <li><a href='home/'>Groups</a></li>
                        <li><a href='home/'>Instagram</a></li>
                        <li><a href='home/'>Local</a></li>
                        <li><a href='home/'>Fundraisers</a></li>
                        <li><a href='home/'>Services</a></li>
                        <li><a href='home/'>About</a></li>
                        <li><a href='home/'>Create Ad</a></li>
                        <li><a href='home/'>Create Page</a></li>
                        <li><a href='home/'>Developers</a></li>
                        <li><a href='home/'>Careers</a></li>
                        <li><a href='home/'>Privacy</a></li>
                        <li><a href='home/'>Cookies</a></li>
                        <li><a href='home/'>Ad Choices</a></li>
                        <li><a href='home/'>Terms</a></li>
                        <li><a href='home/'>HelpSettings</a></li>
                    </ul>
                </div>
                <div className={style.inCorp}>
                    <p>Facebook © 2020</p>
                </div>
            </div>
        </div>
    )
}

export default Footer;
