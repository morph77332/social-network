import React from 'react';
import { connect } from 'react-redux';
import Footer from './Footer';

class FooterContainer extends React.Component {

    render() {

        if (this.props.isAuth) {
            return null
        } else {
            return (
                <Footer />
            )
        }
    }
}

let mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth
})

export default connect(mapStateToProps, { })(FooterContainer);