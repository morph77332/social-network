import React from 'react';
import style from './FormsControls.module.css';

export const TextArea = ({input, meta, ...props}) => {

    const hasError = meta.error && meta.touched;
    return (
        <div className={style.formControl + ' ' + (hasError? style.error : '')}>
            <textarea {...input} {...props} />
            {hasError && <span onMouseOver={onclick}>{meta.error}</span>}
        </div>
    )
}

export const Input = ({ input, meta, ...props }) => {
    const hasError = meta.error && meta.touched;
    return (
        <div className={style.formControlInput + ' ' + (hasError ? style.error : '')}>
            <input {...input} {...props} />
            {/* {hasError && <span onMouseOver={onclick}>{meta.error}</span>} */}
        </div>
    )
}
