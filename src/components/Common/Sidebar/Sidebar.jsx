import React from 'react';
import style from './Sidebar.module.css'
import { NavLink } from 'react-router-dom';
import avatar from './../../../assets/images/user.png'

const Sidebar = (props) => {

    let photoSmall = () => {
        if (props.authUserProfile.photos.small === null) {
            return <img src={avatar} alt='avatar' />
        } else
            return (
                <img src={props.authUserProfile.photos.small} alt='avatar' />
            )
    }

    let profileName = () => {
        if (!props.authUserProfile) {
            return 'loading...'
        } else
            return (
                props.authUserProfile.fullName
                )
    }

    return (
        <div className={style.sideBar}>
            <div className={style.profileInfo}>
                <div>{props.authProfile? photoSmall()
                    : <img src={avatar} alt='avatar' />}</div>
                <h1>{profileName()}</h1>
                <h2>@{profileName()}</h2>
                <div className={style.navigationBlock}>
                    <NavLink to='/home' className={style.hover} activeClassName={style.active} >Home</NavLink>
                    <NavLink to='/about' className={style.hover} activeClassName={style.active}>About</NavLink>
                    <NavLink to='/messenger' className={style.hover} activeClassName={style.active}>Messenger</NavLink>
                    <NavLink to='/friends' className={style.hover} activeClassName={style.active}>Friends</NavLink>
                    <NavLink to='/photos' className={style.hover} activeClassName={style.active}>Photos</NavLink>
                    <NavLink to='/posts' className={style.hover} activeClassName={style.active}>Posts</NavLink>
                    <NavLink to='/community' className={style.hover} activeClassName={style.active}>Community</NavLink>
                    <a href="/" className={style.createButton}>Create a page</a>
                </div>
            </div>
        </div>
    )
}

export default Sidebar;