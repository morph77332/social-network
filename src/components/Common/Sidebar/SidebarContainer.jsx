import React from 'react';
import Sidebar from './Sidebar';
import { connect } from 'react-redux';

class SidebarContainer extends React.Component {
    render() {
        if (this.props.isAuth) {
            return <Sidebar {...this.props} />
        } else {
            return null
        }
    }
}

let mapStateToProps = (state) => ({
    authUserProfile: state.auth.authUserProfile,
    isAuth: state.auth.isAuth
})

export default connect(mapStateToProps, {})(SidebarContainer);