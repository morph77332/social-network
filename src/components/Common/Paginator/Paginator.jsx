import React, { useState } from 'react';
import style from './Paginator.module.css';

let Paginator = ({ totalItemsCount, pageSize, currentPage, onPageChanged }) => {

    let pagesCount = Math.ceil(totalItemsCount / pageSize);
    let pages = [];
    for (let i = 1; i <= pagesCount; i++) {
        pages.push(i)
    }

    const portionSize = 10;
    let portionnCount = Math.ceil(pagesCount / portionSize);
    let [portionNumber, setporionNumber] = useState(1);
    let leftPortionPageNumber = (portionNumber - 1) * portionSize + 1;
    let rightPortionPageNumber = portionNumber * portionSize;

    return (
        <div className={style.paginatorWrapper}>
            <button className={style.buttonPrev} onClick={() => { setporionNumber(portionNumber - 1) }} disabled={portionNumber === 1 && true}>-</button>
            <div className={style.pagesList}>
                {pages.filter(p => p >= leftPortionPageNumber && p <= rightPortionPageNumber).map(p => <span key={p} className={currentPage === p ? style.selectedPage : null} onClick={(e) => { onPageChanged(p) }}>{p}</span>)}
            </div>
            <button className={style.buttonNext} onClick={() => { setporionNumber(portionNumber + 1) }} disabled={portionNumber === portionnCount && true}>-</button>
        </div>
    )
}

export default Paginator;