import React from 'react';
import style from './Notification.module.css'

const Notification = (props) => {
    return (
    <div className={style.notification}><span>{props.number}</span></div>
    )
}

export default Notification;