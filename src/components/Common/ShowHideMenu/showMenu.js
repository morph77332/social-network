export const showMenu = (elem, updateElem) => { // показать выпадающее меню
    if (elem) {
        updateElem(false)
    } else {
        updateElem(true);
    }
};
