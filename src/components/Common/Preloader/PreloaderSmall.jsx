import React from 'react';
import style from './PreloaderSmall.module.css'
import preloader from './../../../assets/images/preloader.gif'

let PreloaderSmall = () => {
    return (
        <div className={style.preloader}>
            <img src={preloader} alt='preloader'/>
        </div>
    )
}

export default PreloaderSmall;