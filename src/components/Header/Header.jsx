import React from 'react';
import style from './Header.module.css';
import Search from './Search/Search';
import MenuConatainer from './Menu/MenuContainer';


const Header = () => {
    
    return (
        <header className={style.header}>
            <Search />
            <MenuConatainer />
        </header>
    )
}

export default Header;