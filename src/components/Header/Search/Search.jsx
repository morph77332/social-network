import React from 'react';
import style from './Search.module.css';

const Search = () => {

    return (
        <div className={style.searchContainer}>
            <div className={style.search}>
                <div className={style.logo} >
                    <a href='/news'>f</a>
                    </div>
                <textarea placeholder='Find something'></textarea>
                <button><img src='https://cdn4.iconfinder.com/data/icons/basic-app/1000/BASICAPP_1-06-512.png' alt='avatar'/></button>
            </div>
        </div>
    )
}

export default Search;