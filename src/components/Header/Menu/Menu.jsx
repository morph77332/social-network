import React from 'react';
import style from './Menu.module.css';
import { NavLink } from 'react-router-dom';
import avatar from './../../../assets/images/user.png'
import PreloaderSmall from '../../Common/Preloader/PreloaderSmall';
import { logOutThunk } from '../../../Redux/AuthReducer';
import { connect } from 'react-redux';
import MenuPart from './MenuPart/MenuPart';
import MenuPart2 from './MenuPart2/MenuPart2';

const Menu = (props) => {

    let showAuthUserInfo = () => {
        if (!props.authUserProfile) {
            return (
                <div className={style.navigation}>
                    <PreloaderSmall />
                    <NavLink to={'/profile/' + props.id}>{props.login}</NavLink>
                    <NavLink to='/news'>Home</NavLink>
                </div>
            )
        } else {
            return (
                <div className={style.navigation}>
                    <NavLink to={`/profile/${props.authUserProfile.userId}/timeline`}>
                        <div className={style.navigationAvatar}>
                            {props.authUserProfile.photos.small
                                ? <img src={props.authUserProfile.photos.small} alt='avatar' />
                                : <img src={avatar} alt='avatar' />}
                        </div>
                    </NavLink>
                    <NavLink to={`/profile/${props.authUserProfile.userId}/timeline`}>{props.login}</NavLink>
                    <NavLink to='/news'>Home</NavLink>
                </div>
            )
        }
    }

    return (
        <div className={style.menuBlock}>
            {props.isAuth ? showAuthUserInfo()
                : <div className={style.navigation}>
                    <NavLink to='/login'>Login</NavLink>
                </div>
            }
            <div className={style.menu}>
                <MenuPart />
                <MenuPart2 logOutThunk={props.logOutThunk}/>
            </div>
        </div>
    )
}

export default connect(null, { logOutThunk })(Menu);