import React from 'react';
import Menu from './Menu';
import { connect } from 'react-redux';
import { setAuthUserProfileThunk } from './../../../Redux/AuthReducer';

class MenuConatainer extends React.Component {

    render() {
        return (
            <Menu {...this.props} />
        )
    }
}

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth,
    login: state.auth.login,
    id: state.auth.userId,
    authUserProfile: state.auth.authUserProfile
});

export default connect(mapStateToProps, { setAuthUserProfileThunk })(MenuConatainer);