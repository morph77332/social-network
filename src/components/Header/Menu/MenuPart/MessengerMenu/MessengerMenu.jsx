import React from 'react';
import style from '../MenuPart.module.css';

const MessengerMenu = (props) => {

    return (
        <div className={style.perseferenceMenu} onMouseLeave={props.deActiveMenu}>
            <div>
                <span>No new messages</span>
            </div>
        </div>
    )
}

export default MessengerMenu;