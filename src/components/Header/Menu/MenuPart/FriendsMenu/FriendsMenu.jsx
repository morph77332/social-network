import React from 'react';
import style from '../MenuPart.module.css';

const FriendsMenu = (props) => {

    return (
        <div className={style.perseferenceMenu} onMouseLeave={props.deActiveMenu}>
            <div>
                <span>No new friends requests</span>
            </div>
        </div>
    )
}

export default FriendsMenu;