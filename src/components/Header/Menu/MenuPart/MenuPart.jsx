import React, { useState } from 'react';
import style from '../Menu.module.css';
import NotificationsMenu from './NotificationsMenu/NotificationsMenu';
import MessengerMenu from './MessengerMenu/MessengerMenu';
import FriendsMenu from './FriendsMenu/FriendsMenu';
import friends from './../../../../assets/images/friends.png';
import friendsSelect from './../../../../assets/images/friends_not.png';
import messenger from './../../../../assets/images/messenger.png';
import messengerSelect from './../../../../assets/images/messenger_not.png';
import notifications from './../../../../assets/images/notifications.png';
import notificationsSelect from './../../../../assets/images/notifications_not.png';
import { showMenu } from '../../../Common/ShowHideMenu/showMenu';
import Notification from '../../../Common/Other/Notification';

const MenuPart = (props) => {

    let [activeFriends, setactiveFriends] = useState(false); // состояние выпадающих меню
    let [activeMessenger, setactiveMessenger] = useState(false);
    let [activeNotifications, setactiveNotifications] = useState(false);
    let [friendsNot, setfriendsNot] = useState(true);
    let [MessengerNot, setMessengerNot] = useState(true);
    let [otherNot, setotherNot] = useState(false);

    // прверка на входящие уведомления
    if (props.friendsRequest) {
        setfriendsNot(true)
    };
    
    if (props.newMessages) {
        setMessengerNot(true)
    };

    if (props.othersNot) {
        setotherNot(true)
    };

    return (
        <div className={style.menuIcons}>
            <div className={style.menuIconsItem}>
                <button onClick={() => { showMenu(activeFriends, setactiveFriends) }}>
                    <img src={activeFriends || friendsNot ? friendsSelect : friends} alt='icon' />
                </button>
                {friendsNot ? <Notification number={1} /> : null}
                {activeFriends ? <FriendsMenu deActiveMenu={() => setactiveFriends(false)} /> : null}
            </div>
            <div className={style.menuIconsItem}>
                <button onClick={() => { showMenu(activeMessenger, setactiveMessenger) }}>
                    <img src={activeMessenger || MessengerNot ? messengerSelect : messenger} alt='icon' />
                </button>
                {MessengerNot ? <Notification number={4}/> : null}
                {activeMessenger ? <MessengerMenu deActiveMenu={() => setactiveMessenger(false)} /> : null}
            </div>
            <div className={style.menuIconsItem}>
                <button onClick={() => { showMenu(activeNotifications, setactiveNotifications) }}>
                    <img src={activeNotifications || otherNot ? notificationsSelect : notifications} alt='icon' />
                </button>
                {otherNot ? <Notification number={1} /> : null}
                {activeNotifications ? <NotificationsMenu deActiveMenu={() => setactiveNotifications(false)} /> : null}
            </div>
        </div>
    )
}

export default MenuPart;