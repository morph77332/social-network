import React from 'react';
import style from '../MenuPart.module.css';

const NotificationsMenu = (props) => {

    return (
        <div className={style.perseferenceMenu} onMouseLeave={props.deActiveMenu}>
            <div>
                <span>No new notifications</span>
            </div>
        </div>
    )
}

export default NotificationsMenu;