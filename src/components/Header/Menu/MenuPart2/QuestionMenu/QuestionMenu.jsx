import React from 'react';
import style from '../../MenuPart/MenuPart.module.css';

const QuestionMenu = ({ deActiveMenu}) => {

    return (
        <div className={style.perseferenceMenu} onMouseLeave={deActiveMenu}>
            <div>
                <span>Some info</span>
            </div>
        </div>
    )
}

export default QuestionMenu;