import React, { useState } from 'react';
import style from '../Menu.module.css';
import PerseferenceMenu from './PerseferenceMenu/PerseferenceMenu';
import QuestionMenu from './QuestionMenu/QuestionMenu';
import question from './../../../../assets/images/question.png';
import questionSelect from './../../../../assets/images/question_select.png';
import arrow from './../../../../assets/images/arrow_header.png';
import arrowSelected from './../../../../assets/images/arrow_header_select.png';
import { showMenu } from '../../../Common/ShowHideMenu/showMenu';

const MenuPart2 = (props) => {

    let [activeQuestion, setactiveQuestion] = useState(false);          // состояние выпадающих меню
    let [activeSettings, setactiveSettings] = useState(false);

    return (
        <div className={style.menuIcons2}>
            <div className={style.menuIconsItem}>
                <button onClick={() => { showMenu(activeQuestion, setactiveQuestion) }}>
                    <img src={activeQuestion ? questionSelect : question} alt='icon' />
                </button>
                {activeQuestion ? <QuestionMenu logOutThunk={props.logOutThunk} deActiveMenu={() => setactiveQuestion(false)} /> : null}
            </div>
            <div className={style.menuIconsItem}>
                <button className={style.menu2Per} onClick={() => { showMenu(activeSettings, setactiveSettings) }}>
                    <img src={activeSettings ? arrowSelected : arrow} alt='icon' />
                </button>
                {activeSettings ? <PerseferenceMenu logOutThunk={props.logOutThunk} deActiveMenu={() => setactiveSettings(false)} /> : null}
            </div>
        </div>
    )
}

export default MenuPart2;