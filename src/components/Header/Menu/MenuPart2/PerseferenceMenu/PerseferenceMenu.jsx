import React from 'react';
import style from '../../MenuPart/MenuPart.module.css';

const PerseferenceMenu = ({ deActiveMenu, logOutThunk }) => {

    const logOutButton = () => {
        logOutThunk();
    }

    return (
        <div className={style.perseferenceMenu} onMouseLeave={deActiveMenu}>
            <div>
                <span>Change groups</span>
            </div>
            <div>
                <span>Advertising on SN</span>
            </div>
            <div>
                <span>Activity Log</span>
            </div>
            <div>
                <span>News Feed preseferences</span>
            </div>
            <div>
                <span>Settings</span>
            </div>
            <div>
                <span onClick={logOutButton}>Log Out</span>
            </div>
        </div>
    )
}

export default PerseferenceMenu;