import React from 'react';
import loginHeader from './LoginHeader.module.css';
import logo from './../../assets/images/logo.png';
import { Field, reduxForm } from 'redux-form';
import { Input } from '../Common/FormsControls/FormsControls';
import { required } from '../../utils/validators/validators';
import { loginThunk } from '../../Redux/AuthReducer';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import style from './../Common/FormsControls/FormsControls.module.css';

const LoginForm = ({ handleSubmit, error}) => {

    return (
        <form className={loginHeader.authBlock} onSubmit={handleSubmit}>
            <div className={loginHeader.authBlockItem}>
                <span className={loginHeader.authBlockItemText}>Email</span>
                <Field component={Input} validate={[required]} name={'email'} type="text" />
                <div className={loginHeader.authBlockCheck}>
                    {error
                        ? <span className={style.formSummaryError}>{error}</span>
                        : <><Field type="checkbox" name={'rememberMe'} component={'input'} className={loginHeader.check} />
                            <span>Keep me logged in</span></>
                    }
                </div>
            </div>
            <div className={loginHeader.authBlockItem}>
                <span className={loginHeader.authBlockItemText}>Password</span>
                <Field component={Input} validate={[required]} name={'password'} type="password" />
                <div className={loginHeader.authBlockCheck}>
                    <span>Forgot your password?</span>
                </div>
            </div>
            <button type='submit'>Log In</button>
        </form>
    )
}

const LoginReduxForm = reduxForm({ form: 'login' })(LoginForm)

class LoginHeader extends React.Component {

    onSubmit = (formData) => {
        this.props.loginThunk(formData.email, formData.password, formData.rememberMe)
    }

    render() {

        if (this.props.isAuth) {
            return <Redirect to={'/news'} />
        }
        return (
            <header className={loginHeader.header}>
                <div className={loginHeader.logo}>
                    <a href='/news'>
                        <img src={logo} alt='logo' ></img></a>
                </div>
                <LoginReduxForm onSubmit={this.onSubmit} />
            </header>
        )
    }
}

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth,
});


export default connect(mapStateToProps, { loginThunk })(LoginHeader)