import React from 'react';
import Header from './Header';
import { connect } from 'react-redux';
import LoginHeader from './LoginHeader';

class HeaderContainer extends React.Component {
    render() {
        if (this.props.isAuth) {
            return <Header />
        } else {
            return <LoginHeader />
        }
    }
}

let mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth
})

export default connect(mapStateToProps, {  })(HeaderContainer);
