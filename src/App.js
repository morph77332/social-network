import React from 'react';
import './App.css';
import { Route, BrowserRouter, withRouter } from 'react-router-dom';
import { initialApp } from './Redux/AppReducer';
import { connect, Provider } from 'react-redux';
import { compose } from 'redux';
import store from './Redux/ReduxStore';
import UsersContainer from './components/Main/Users/UsersContainer';
import Community from './components/Main/Community/Community';
import MessengerContainer from './components/Main/Messenger/MessengerContainer';
import LoginContainer from './components/Main/Login/LoginContainer';
import HeaderContainer from './components/Header/HeaderContainer';
import FooterContainer from './components/Footer/FooterContainer';
import NotFound from './components/Main/NotFound/NotFound';
import PreloaderSmall from './components/Common/Preloader/PreloaderSmall';
import NewsContainer from './components/Main/News/NewsContainer';
import ProfileContainerHook from './components/Main/Profile/ProfileContainerHook';

class App extends React.Component {

  componentDidMount() { // инициализация приложения
    this.props.initialApp()
  }

  render() {
    if (!this.props.initialised) {
      return (
        <div className={'preloaderMainPage'}>
          <PreloaderSmall />
        </div>
      )
    }
    else {
      return (
        <div className="appWrapper">
          <div className='headerContainer'>
            <HeaderContainer />
          </div>
          <main>
            <Route exact path='/' render={() => <NewsContainer />} />
            <Route path='/news' render={() => <NewsContainer />} />
            <Route path='/profile/:userId?' render={() => <ProfileContainerHook />} />
            <Route path='/messenger' render={() => <MessengerContainer />} />
            <Route path='/users' render={() => <UsersContainer />} />
            <Route path='/community' render={() => <Community />} />
            <Route path='/login' render={() => <LoginContainer />} />
          </main>
          <footer>
            <FooterContainer />
          </footer>
        </div>
      );
    }
  }
}
const mapStateToProps = (state) => ({
  initialised: state.app.initialised
})


const AppContainer = compose(connect(mapStateToProps, { initialApp }), withRouter)(App);

const MainApp = () => {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <AppContainer />
      </Provider>
    </BrowserRouter>
  )
}

export default MainApp;
