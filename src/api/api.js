import * as axios from "axios";

const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        'API-KEY': '5fbc1a07-efdb-4f76-a1a1-ae5c467f8691'
    }
});

export const usersAPI = {
    getUsers(currentPage = 1, pageSize = 6) {
        return instance.get(`users?page=${currentPage}&count=${pageSize}`).then(response => {
            return response.data
        });
    },
    follow(id) {
        return instance.post(`follow/${id}`)
    },
    unfollow(id) {
        return instance.delete(`follow/${id}`)
    }
}

export const profileAPI = {
    getProfile(userId) {
        return instance.get(`profile/${userId}`).then(response => {
            return response.data
        });
    },
    getStatus(id) {
        return instance.get(`profile/status/${id}`)
    },
    updateStatus(status) {
        return instance.put(`profile/status`, { status })
    },
    savePhoto(filePhoto) {
        const formData = new FormData();
        formData.append('image', filePhoto);
        return instance.put(`profile/photo`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
    },
    saveProfile(profile) {
        return instance.put(`profile`,  profile )
    }
}

export const authAPI = {
    login(email, password, rememberMe) {
        return instance.post(`auth/login`, { email, password, rememberMe })
    },
    logOut() {
        return instance.delete(`auth/login`)
    },
    me() {
        return instance.get(`auth/me`).then(response => {
            return response.data
        });
    },
    getAuthProfile(id) {
        return instance.get(`profile/${id}`).then(response => {
            return response.data
        });
    }

}

export const newsAPI = {
    getNews() {
        return axios.get('http://newsapi.org/v2/top-headlines?country=us&apiKey=a99759da61184c37b667bf0a87492707').then(response => {
            return response.data.articles
        })
    }
}


